<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 19. 02. 2019
 */

declare(strict_types=1);

namespace App\Command;

use App\Services\AtlasService\AtlasServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateAtlasesCommand
 * @package Sovanet\MinirolCoreBundle\Command
 */
final class GenerateAtlasesCommand extends Command
{
    /**
     * @var AtlasServiceInterface
     */
    private $atlasService;

    /**
     * GenerateAtlasesCommand constructor.
     * @param AtlasServiceInterface $atlasService
     */
    public function __construct(AtlasServiceInterface $atlasService)
    {
        parent::__construct();
        $this->atlasService = $atlasService;
    }

    protected function configure()
    {
        $this
            ->setAliases(['c:g:a'])
            ->setName('cron:generate:atlases')
            ->setDescription('Generate archetype atlases');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(sprintf('Generate atlases ....'));
        $this->atlasService->generateAtlases();
        $output->writeln(sprintf('Finished'));
    }

}
