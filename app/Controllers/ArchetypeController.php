<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\ArchetypeRepositoryInterface;
use App\Model\ValueObject\ArchetypeValueObject;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ArchetypeController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArchetypeController extends DefaultController
{

    /**
     * @var ArchetypeRepositoryInterface
     */
    private $archetypeRepository;

    /**
     * ArchetypeController constructor.
     * @param ArchetypeRepositoryInterface $archetypeRepository
     */
    public function __construct(ArchetypeRepositoryInterface $archetypeRepository)
    {
        $this->archetypeRepository = $archetypeRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $entity = $this->archetypeRepository->findArchetype(intval($args['id']));
                return $response->withJson(new ArchetypeValueObject($entity), 200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }

}
