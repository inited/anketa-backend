<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Archetype;
use App\Model\Entity\Locale;
use App\Model\ValueObject\ArchetypeValueObject;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ArchetypeCreateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArchetypeCreateController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ArchetypeCreateController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        if (is_array($data) && count($data)) {
            $entity = new Archetype();

            if (array_key_exists('description_dk', $data)) {
                $translation = $entity->getTranslation(Locale::LOCALE_DK);
                $translation->setDescription($data['description_dk']);
            }

            if (array_key_exists('description_en', $data)) {
                $translation = $entity->getTranslation(Locale::LOCALE_EN);
                $translation->setDescription($data['description_en']);
            }

            if (array_key_exists('image', $data)) {
                $entity->setImage($data['image']);
                $entity->setImageModified(new \DateTime());
            }

            if (array_key_exists('qr_code', $data)) {
                $entity->setQrCode($data['qr_code']);
            }

            if (array_key_exists('sound', $data)) {
                $entity->setSound($data['sound']);
                $entity->setSoundModified(new \DateTime());
            }

            if (array_key_exists('thumbnail', $data)) {
                $entity->setThumbnail($data['thumbnail']);
                $entity->setThumbnailModified(new \DateTime());
            }

            if (array_key_exists('title_dk', $data)) {
                $translation = $entity->getTranslation(Locale::LOCALE_DK);
                $translation->setTitle($data['title_dk']);
            }

            if (array_key_exists('title_en', $data)) {
                $translation = $entity->getTranslation(Locale::LOCALE_EN);
                $translation->setTitle($data['title_en']);
            }

            if (array_key_exists('video', $data)) {
                $entity->setVideo($data['video']);
                $entity->setVideoModified(new \DateTime());
            }

            $this->em->persist($entity);
            $this->em->flush();

            return $response->withJson(new ArchetypeValueObject($entity), 200);

        }

        return $response->withStatus(400);
    }

}
