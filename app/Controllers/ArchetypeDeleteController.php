<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\ArchetypeRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class SegmentAddEditController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArchetypeDeleteController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ArchetypeRepositoryInterface
     */
    private $archetypeRepository;

    /**
     * ArchetypeDeleteController constructor.
     * @param EntityManagerInterface $em
     * @param ArchetypeRepositoryInterface $archetypeRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        ArchetypeRepositoryInterface $archetypeRepository
    )
    {
        $this->archetypeRepository = $archetypeRepository;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $entity = $this->archetypeRepository->findArchetype(intval($args['id']));
                $entity->setDeleted(true);
                $entity->setImageModified(new \DateTime()); // Should be for new atlases generate
                $this->em->flush();

                return $response->withStatus(200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }

}
