<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Locale;
use App\Model\Repository\ArchetypeRepositoryInterface;
use App\Model\ValueObject\ArchetypeValueObject;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ArchetypeUpdateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArchetypeUpdateController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ArchetypeRepositoryInterface
     */
    private $archetypeRepository;

    /**
     * ArchetypeUpdateController constructor.
     * @param EntityManagerInterface $em
     * @param ArchetypeRepositoryInterface $archetypeRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        ArchetypeRepositoryInterface $archetypeRepository
    )
    {
        $this->archetypeRepository = $archetypeRepository;
        $this->em = $em;

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $data = $request->getParsedBody();
                $entity = $this->archetypeRepository->findArchetype(intval($args['id']));

                if (is_array($data) && count($data)) {
                    if (array_key_exists('description_dk', $data)) {
                        $translation = $entity->getTranslation(Locale::LOCALE_DK);
                        $translation->setDescription($data['description_dk']);
                    }

                    if (array_key_exists('description_en', $data)) {
                        $translation = $entity->getTranslation(Locale::LOCALE_EN);
                        $translation->setDescription($data['description_en']);
                    }

                    if (array_key_exists('image', $data)) {
                        $entity->setImage($data['image']);
                    }

                    if (array_key_exists('qr_code', $data)) {
                        $entity->setQrCode($data['qr_code']);
                    }

                    if (array_key_exists('sound', $data)) {
                        $entity->setSound($data['sound']);
                    }

                    if (array_key_exists('thumbnail', $data)) {
                        $entity->setThumbnail($data['thumbnail']);
                        $entity->setThumbnailModified(new \DateTime());
                    }

                    if (array_key_exists('title_dk', $data)) {
                        $translation = $entity->getTranslation(Locale::LOCALE_DK);
                        $translation->setTitle($data['title_dk']);
                    }

                    if (array_key_exists('title_en', $data)) {
                        $translation = $entity->getTranslation(Locale::LOCALE_EN);
                        $translation->setTitle($data['title_en']);
                    }

                    if (array_key_exists('video', $data)) {
                        $entity->setVideo($data['video']);
                    }
                }

                $this->em->flush();

                return $response->withJson(new ArchetypeValueObject($entity), 200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }
}
