<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Repository\ArchetypeRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ModuleController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArchetypesController extends DefaultController
{

    /**
     * @var ArchetypeRepositoryInterface
     */
    private $archetypeRepository;

    /**
     * ArchetypesController constructor.
     * @param ArchetypeRepositoryInterface $archetypeRepository
     */
    public function __construct(ArchetypeRepositoryInterface $archetypeRepository)
    {
        $this->archetypeRepository = $archetypeRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        return $response->withJson($this->archetypeRepository->findAllArchetypes(), 200);
    }
}
