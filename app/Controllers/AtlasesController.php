<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Atlas;
use App\Model\Repository\AtlasRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ModuleController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class AtlasesController extends DefaultController
{

    /**
     * @var AtlasRepositoryInterface
     */
    private $atlasRepository;

    /**
     * AtlasesController constructor.
     * @param AtlasRepositoryInterface $atlasRepository
     */
    public function __construct(AtlasRepositoryInterface $atlasRepository)
    {
        $this->atlasRepository = $atlasRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        $atlases = $this->atlasRepository->findAll();

        $data = [];

        /** @var Atlas $atlas */
        foreach ($atlases as $atlas) {
            $data['modified'] = $atlas->getModified()->getTimestamp();
            $data['atlas'][] = $atlas->getImageAtlasLink();
            $data['xml'][] = $atlas->getXmlAtlasLink();
        }

        return $response->withJson($data, 200);
    }
}
