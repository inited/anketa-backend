<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 19. 02. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Services\AtlasService\AtlasServiceException;
use App\Services\AtlasService\AtlasServiceInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AtlasesGenerateController
 * @package App\Controllers
 */
final class AtlasesGenerateController extends DefaultController
{

    /**
     * @var AtlasServiceInterface
     */
    private $atlasService;

    /**
     * AtlasesGenerateController constructor.
     * @param AtlasServiceInterface $atlasService
     */
    public function __construct(AtlasServiceInterface $atlasService)
    {
        $this->atlasService = $atlasService;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        try {
            $this->atlasService->generateAtlases();
            return $response->withStatus(200);
        } catch (AtlasServiceException $e) {
            return $response->withJson($e->getMessage(), 500);
        }
    }
}
