<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Archetype
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Entity()
 * @ORM\Table(name="archetype__archetype")
 * @ORM\HasLifecycleCallbacks()
 */
class Archetype
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted = false;

    /**
     * @var null|string
     * @ORM\Column(name="image", type="string", nullable=true)
     */
    private $image;

    /**
     * @var null|\DateTimeInterface
     * @ORM\Column(name="image_modified", type="datetime", nullable=true)
     */
    private $imageModified;

    /**
     * @var string
     * @ORM\Column(name="sound", type="string", nullable=true)
     */
    private $sound = '';

    /**
     * @var null|\DateTimeInterface
     * @ORM\Column(name="sound_modified", type="datetime", nullable=true)
     */
    private $soundModified;

    /**
     * @var null|string
     * @ORM\Column(name="thumbnail", type="string", nullable=true)
     */
    private $thumbnail;

    /**
     * @var null|\DateTimeInterface
     * @ORM\Column(name="thumbnail_modified", type="datetime", nullable=true)
     */
    private $thumbnailModified;

    /**
     * @var ArrayCollection|ArchetypeTranslation[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\ArchetypeTranslation", mappedBy="translatable", indexBy="locale", cascade={"persist", "merge"})
     */
    private $translations;

    /**
     * @var null|string
     * @ORM\Column(name="qr_code", type="string", nullable=true)
     */
    private $qrCode;

    /**
     * @var null|string
     * @ORM\Column(name="video", type="string", nullable=true)
     */
    private $video;

    /**
     * @var null|\DateTimeInterface
     * @ORM\Column(name="video_modified", type="datetime", nullable=true)
     */
    private $videoModified;

    /**
     * Archetype constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * Check if media files has changed
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function checkMediaChanges(LifecycleEventArgs $event)
    {
        $changeSet = $event
            ->getEntityManager()
            ->getUnitOfWork()
            ->getEntityChangeSet($this);

        if (0 < count($changeSet)) {
            if (array_key_exists('image', $changeSet)) {
                $this->imageModified = new \DateTime();
            }

            if (array_key_exists('sound', $changeSet)) {
                $this->soundModified = new \DateTime();
            }

            if (array_key_exists('thumbnail', $changeSet)) {
                $this->thumbnailModified = new \DateTime();
            }

            if (array_key_exists('video', $changeSet)) {
                $this->videoModified = new \DateTime();
            }
        }

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     * @throws \Exception
     */
    public function setImage(?string $image)
    {
        $this->image = $image;
    }

    /**
     * @return null|\DateTimeInterface
     */
    public function getImageModified()
    {
        return $this->imageModified;
    }

    /**
     * @param \DateTimeInterface|null $imageModified
     */
    public function setImageModified(?\DateTimeInterface $imageModified): void
    {
        $this->imageModified = $imageModified;
    }

    /**
     * @return null|\DateTimeInterface
     */
    public function getSoundModified()
    {
        return $this->soundModified;
    }

    /**
     * @param \DateTimeInterface|null $soundModified
     */
    public function setSoundModified(?\DateTimeInterface $soundModified): void
    {
        $this->soundModified = $soundModified;
    }

    /**
     * @return string|null
     */
    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    /**
     * @param string|null $thumbnail
     */
    public function setThumbnail(?string $thumbnail): void
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getThumbnailModified(): ?\DateTimeInterface
    {
        return $this->thumbnailModified;
    }

    /**
     * @param \DateTimeInterface|null $thumbnailModified
     */
    public function setThumbnailModified(?\DateTimeInterface $thumbnailModified): void
    {
        $this->thumbnailModified = $thumbnailModified;
    }

    /**
     * @return string|null
     */
    public function getSound()
    {
        return $this->sound;
    }

    /**
     * @param string|null $sound
     * @throws \Exception
     */
    public function setSound(?string $sound)
    {
        $this->sound = $sound;
    }

    /**
     * @param string $locale
     * @return ArchetypeTranslation|mixed|null
     */
    public function getTranslation(string $locale)
    {
        $locale = strtolower($locale);
        $translation = $this->translations->get($locale);

        if (null === $translation) {
            $translation = new ArchetypeTranslation();
            $translation->setLocale($locale);
            $translation->setTranslatable($this);
            $this->translations->set($locale, $translation);
        }

        return $translation;
    }

    /**
     * @return ArchetypeTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations->toArray();
    }

    /**
     * @return string|null
     */
    public function getQrCode()
    {
        return $this->qrCode;
    }

    /**
     * @param string|null $qrCode
     */
    public function setQrCode(?string $qrCode)
    {
        $this->qrCode = $qrCode;
    }

    /**
     * @return string|null
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param string|null $video
     * @throws \Exception
     */
    public function setVideo(?string $video)
    {
        $this->video = $video;
    }

    /**
     * @return  null|\DateTimeInterface
     */
    public function getVideoModified()
    {
        return $this->videoModified;
    }

    /**
     * @param \DateTimeInterface|null $videoModified
     */
    public function setVideoModified(?\DateTimeInterface $videoModified): void
    {
        $this->videoModified = $videoModified;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }


}
