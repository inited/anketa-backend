<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Atlas
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="atlas__atlas")
 * @ORM\EntityListeners({"App\Model\EntityListener\AtlasEntityListener"})
 */
class Atlas
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="image_atlas", type="string")
     */
    private $imageAtlas = '';

    /**
     * @var string
     * @ORM\Column(name="xml_atlas", type="string")
     */
    private $xmlAtlas = '';

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * Download URI
     * @var string
     */
    private $uri;

    /**
     * Atlas constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->modified = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImageAtlas(): string
    {
        return $this->imageAtlas;
    }

    /**
     * @param string $imageAtlas
     */
    public function setImageAtlas(string $imageAtlas): void
    {
        $this->imageAtlas = $imageAtlas;
    }

    /**
     * @return string
     */
    public function getXmlAtlas(): string
    {
        return $this->xmlAtlas;
    }

    /**
     * @param string $xmlAtlas
     */
    public function setXmlAtlas(string $xmlAtlas): void
    {
        $this->xmlAtlas = $xmlAtlas;
    }

    /**
     * @param string $uri
     */
    public function setAtlasDownloadUrl(string $uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getModified(): \DateTimeInterface
    {
        return $this->modified;
    }

    /**
     * @param \DateTimeInterface $modified
     */
    public function setModified(\DateTimeInterface $modified): void
    {
        $this->modified = $modified;
    }

    /**
     * @return string
     */
    public function getXmlAtlasLink()
    {
        return $this->uri . $this->xmlAtlas;
    }

    /**
     * @return string
     */
    public function getImageAtlasLink()
    {
        return $this->uri . $this->imageAtlas;
    }

}
