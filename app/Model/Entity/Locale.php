<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Locale
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="system__locale")
 */
class Locale
{

    const LOCALE_DK = 'dk';
    const LOCALE_EN = 'en';

    /**
     * @var string
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     */
    private $id;

    /**
     * @return array
     */
    public static function getLocales()
    {
        return [
            self::LOCALE_DK,
            self::LOCALE_EN
        ];
    }
    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
