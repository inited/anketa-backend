<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class RoleEntity
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="system__role")
 * @ORM\Entity(readOnly=true)
 */
class RoleEntity
{

    /**
     * @var string
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(name="default", type="boolean")
     */
    private $default = false;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->default;
    }
}
