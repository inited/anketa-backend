<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Class TokenEntity
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="system__token", indexes={@ORM\Index(columns={"valid"})})
 * @ORM\Entity()
 */
class TokenEntity
{

    /**
     * @var string
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var null|\DateTime
     * @ORM\Column(name="invalidation_date", type="datetime", nullable=true)
     */
    private $invalidationDate;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="UserEntity", inversedBy="tokens")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var bool
     * @ORM\Column(name="valid", type="boolean")
     */
    private $valid;

    /**
     * TokenEntity constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->created = new \DateTime();
        $this->valid = true;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return UserEntity
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     */
    public function setUser(UserEntity $user)
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @param bool $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
        if (false === $valid) {
            $this->invalidationDate = new \DateTime();
        }
    }

}
