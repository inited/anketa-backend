<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Class UserPasswordRequestEntity
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="user__password_request")
 * @ORM\Entity()
 */
class UserPasswordRequestEntity
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(name="hash", type="string")
     */
    private $hash;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="UserEntity")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var boolean
     * @ORM\Column(name="valid", type="boolean")
     */
    private $valid;

    /**
     * @var \DateTime
     * @ORM\Column(name="valid_to", type="datetime")
     */
    private $validTo;

    /**
     * UserPasswordRequestEntity constructor.
     * @param UserEntity $user
     */
    public function __construct(UserEntity $user)
    {
        $this->created = new \DateTime();
        $this->hash = Uuid::uuid4()->toString();
        $this->user = $user;
        $this->valid = true;
        $this->validTo = new \DateTime('+24hours');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @return UserEntity
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param bool $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }
}
