<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\EntityListener;

use App\Model\Entity\Atlas;

/**
 * Class AtlasEntityListener
 * @package App\Model\EntityListener
 */
final class AtlasEntityListener
{

    /**
     * @var string $fullUrl
     */
    private $atlasesDirectory;

    /**
     * @var string
     */
    private $atlasDownloadUrl;

    /**
     * AtlasEntityListener constructor.
     * @param string $atlasesDirectory
     * @param string $atlasDownloadUrl
     */
    public function __construct(
        string $atlasesDirectory,
        string $atlasDownloadUrl
    )
    {
        $this->atlasesDirectory = rtrim($atlasesDirectory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->atlasDownloadUrl = rtrim($atlasDownloadUrl, '/') . '/';
    }

    /**
     * @param Atlas $entity
     */
    public function postLoad(Atlas $entity)
    {
        $entity->setAtlasDownloadUrl($this->atlasDownloadUrl);
    }

    /**
     * @param Atlas $entity
     */
    public function postRemove(Atlas $entity)
    {
        $imagePath = $this->atlasesDirectory . $entity->getImageAtlas();
        $xmlPath = $this->atlasesDirectory . $entity->getXmlAtlas();

        if (is_file($imagePath)) {
            unlink($imagePath);
        }

        if (is_file($xmlPath)) {
            unlink($xmlPath);
        }

    }
}
