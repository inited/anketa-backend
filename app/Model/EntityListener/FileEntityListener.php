<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\EntityListener;

use App\Model\Entity\FileEntityAwareInterface;

/**
 * Class FileEntityListener
 * @package App\Model\EntityListener
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class FileEntityListener
{

    /**
     * @var string $fullUrl
     */
    private $fullUrl;

    /**
     * FileEntityListener constructor.
     * @param string $fullUrl
     */
    public function __construct(string $fullUrl)
    {
        $this->fullUrl = $fullUrl;
    }

    /**
     * @param FileEntityAwareInterface $fileEntity
     */
    public function postLoad(FileEntityAwareInterface $fileEntity)
    {
        $fileEntity->setServerUrl($this->fullUrl);
    }
}
