<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Archetype;
use App\Model\Entity\Locale;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class UserRepository
 * @package App\Model\Repository\User
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArchetypeRepository extends BaseRepository implements ArchetypeRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findLatestImageModifiedDate(): \DateTimeInterface
    {
        $date = '1970-01-01 00:00:01';

        try {
            $date = $this->_em->createQueryBuilder()
                ->select('MAX(archetype.imageModified) date')
                ->from(Archetype::class, 'archetype')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            // Do nothing;
        }

        return new \DateTime($date);
    }

    /**
     * {@inheritdoc}
     */
    public function findArchetype(int $id): Archetype
    {
        try {
            $archetype = $this->_em->createQueryBuilder()
                ->select('archetype')
                ->from(Archetype::class, 'archetype')
                ->andWhere('archetype.id = :id')
                ->andWhere('archetype.deleted = :deleted')
                ->setParameter('id', $id)
                ->setParameter('deleted', false)
                ->getQuery()
                ->getOneOrNullResult();

            if ($archetype instanceof Archetype) {
                return $archetype;
            }

        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllArchetypes(): array
    {
        return $this->_em->createQueryBuilder()
            ->select('archetype.id')
            ->addSelect('archetype.image', 'UNIX_TIMESTAMP(archetype.imageModified) imageModified')
            ->addSelect('archetype.qrCode')
            ->addSelect('archetypeDk.description description_dk', 'archetypeEn.description description_en')
            ->addSelect('archetype.sound', 'UNIX_TIMESTAMP(archetype.soundModified) soundModified')
            ->addSelect('archetypeDk.title title_dk', 'archetypeEn.title title_en')
            ->addSelect('archetype.thumbnail', 'UNIX_TIMESTAMP(archetype.thumbnailModified) thumbnail_modified')
            ->addSelect('archetype.video', 'UNIX_TIMESTAMP(archetype.videoModified) videoModified')
            ->from(Archetype::class, 'archetype')
            ->leftJoin('archetype.translations', 'archetypeDk', 'WITH', 'archetypeDk.locale = :dk')
            ->leftJoin('archetype.translations', 'archetypeEn', 'WITH', 'archetypeEn.locale = :en')
            ->andWhere('archetype.deleted = :deleted')
            ->setParameter('deleted', false)
            ->setParameter('dk', Locale::LOCALE_DK)
            ->setParameter('en', Locale::LOCALE_EN)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
