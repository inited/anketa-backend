<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Archetype;

/**
 * Interface ArchetypeRepositoryInterface
 * @package App\Model\Repository
 */
interface ArchetypeRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param int $id
     * @return Archetype
     * @throws EntityNotFoundException
     */
    public function findArchetype(int $id): Archetype;

    /**
     * @return array
     */
    public function findAllArchetypes(): array;

    /**
     * @return \DateTimeInterface
     */
    public function findLatestImageModifiedDate(): \DateTimeInterface;
}
