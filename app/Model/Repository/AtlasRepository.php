<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 19. 02. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Atlas;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class AtlasRepository
 * @package App\Model\Repository
 */
class AtlasRepository extends BaseRepository implements AtlasRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findLatestModifiedDate(): \DateTimeInterface
    {
        $date = '1970-01-01 00:00:01';

        try {
            $lastDate = $this->_em->createQueryBuilder()
                ->select('MAX(atlas.modified) date')
                ->from(Atlas::class, 'atlas')
                ->getQuery()
                ->getSingleScalarResult();

            if (null !== $lastDate) {
                $date = $lastDate;
            }

        } catch (NonUniqueResultException $e) {
            // Do nothing;
        }

        return new \DateTime($date);
    }
}
