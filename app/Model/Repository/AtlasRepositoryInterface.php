<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 19. 02. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Interface AtlasRepositoryInterface
 * @package App\Model\Repository
 */
interface AtlasRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return \DateTimeInterface
     */
    public function findLatestModifiedDate(): \DateTimeInterface;
}
