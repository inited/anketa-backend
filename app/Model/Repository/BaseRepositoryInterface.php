<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Interface BaseRepositoryInterface
 * @package App\Model\Repository
 */
interface BaseRepositoryInterface extends ObjectRepository
{

    /**
     * @param mixed $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return null|object
     * @throws EntityNotFoundException
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param mixed $object
     * @return mixed
     */
    public function persistEntity($object);

    /**
     * @param array $entities
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveEntities(array $entities);

    /**
     * @param mixed $object
     * @return mixed
     */
    public function saveEntity($object);
}
