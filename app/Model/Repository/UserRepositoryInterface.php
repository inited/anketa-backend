<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InactiveUserException;
use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\UserDeletedException;
use App\Model\Entity\UserEntity;

/**
 * Interface UserRepositoryInterface
 * @package App\Model\Repository
 */
interface UserRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param UserEntity $userEntity
     * @return mixed
     */
    public function invalidUserPasswordChangeRequests(UserEntity $userEntity);

    /**
     * @param bool $onlyActive
     * @return array
     */
    public function findAllUsers($onlyActive = false);

    /**
     * @param mixed $email
     * @return null|object
     * @throws EntityNotFoundException
     */
    public function findByEmail($email);

    /**
     * @param mixed $key
     * @return mixed
     * @throws EntityNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findValidPasswordCheckKey($key);

    /**
     * @param mixed $email
     * @param mixed $password
     * @return UserEntity
     * @throws EntityNotFoundException
     * @throws InactiveUserException
     * @throws InvalidCredentialsException
     * @throws UserDeletedException
     */
    public function verifyUser($email, $password);
}
