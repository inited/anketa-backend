<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

use App\Model\Entity\Archetype;

/**
 * Class ArchetypeValueObject
 * @package App\Model\ValueObject
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class ArchetypeValueObject
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $description_dk = '';

    /**
     * @var string
     */
    public $description_en = '';

    /**
     * @var string|null
     */
    public $image;

    /**
     * @var int
     */
    public $image_modified;

    /**
     * @var string|null
     */
    public $qr_code;

    /**
     * @var string|null
     */
    public $sound;

    /**
     * @var int
     */
    public $sound_modified;

    /**
     * @var string|null
     */
    public $thumbnail;

    /**
     * @var int
     */
    public $thumbnail_modified;

    /**
     * @var null|string
     */
    public $title_en;

    /**
     * @var null|string
     */
    public $title_dk;

    /**
     * @var string|null
     */
    public $video;

    /**
     * @var int
     */
    public $video_modified;


    /**
     * ArchetypeValueObject constructor.
     * @param Archetype $archetype
     */
    public function __construct(Archetype $archetype)
    {
        $this->id = $archetype->getId();
        $this->image = $archetype->getImage();
        $this->image_modified = null === $archetype->getImageModified() ?
            null : $archetype->getImageModified()->getTimestamp();
        $this->qr_code = $archetype->getQrCode();
        $this->sound = $archetype->getSound();
        $this->sound_modified = null === $archetype->getSoundModified() ?
            null : $archetype->getSoundModified()->getTimestamp();
        $this->thumbnail = $archetype->getThumbnail();
        $this->thumbnail_modified = null === $archetype->getThumbnailModified() ?
            null : $archetype->getThumbnailModified()->getTimestamp();
        $this->video = $archetype->getVideo();
        $this->video_modified = null === $archetype->getVideoModified() ?
            null : $archetype->getVideoModified()->getTimestamp();

        foreach ($archetype->getTranslations() as $translation) {
            $descriptionIdentifier = 'description_' . $translation->getLocale();
            $titleIdentifier = 'title_' . $translation->getLocale();
            $this->{$descriptionIdentifier} = $translation->getDescription();
            $this->{$titleIdentifier} = $translation->getTitle();
        }

    }
}
