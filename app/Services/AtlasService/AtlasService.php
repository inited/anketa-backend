<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 19. 02. 2019
 */

declare(strict_types=1);

namespace App\Services\AtlasService;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Atlas;
use App\Model\Entity\FileEntity;
use App\Model\Repository\ArchetypeRepositoryInterface;
use App\Model\Repository\AtlasRepositoryInterface;
use App\Model\Repository\FileRepositoryInterface;
use App\Services\ImageCombiner\ImageCombinerException;
use App\Services\ImageCombiner\ImageCombinerServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AtlasService
 * @package App\Services\AtlasService
 */
class AtlasService implements AtlasServiceInterface
{
    /**
     * @var ArchetypeRepositoryInterface
     */
    private $archetypeRepository;

    /**
     * @var AtlasRepositoryInterface
     */
    private $atlasRepository;

    /**
     * @var string
     */
    private $cdnPath;

    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ImageCombinerServiceInterface
     */
    private $imageCombinerService;

    /**
     * AtlasService constructor.
     * @param string $cdnPath
     * @param ArchetypeRepositoryInterface $archetypeRepository
     * @param AtlasRepositoryInterface $atlasRepository
     * @param EntityManagerInterface $em
     * @param FileRepositoryInterface $fileRepository
     * @param ImageCombinerServiceInterface $imageCombinerService
     */
    public function __construct(
        string $cdnPath,
        ArchetypeRepositoryInterface $archetypeRepository,
        AtlasRepositoryInterface $atlasRepository,
        EntityManagerInterface $em,
        FileRepositoryInterface $fileRepository,
        ImageCombinerServiceInterface $imageCombinerService
    )
    {
        $this->archetypeRepository = $archetypeRepository;
        $this->atlasRepository = $atlasRepository;
        $this->cdnPath = rtrim($cdnPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->fileRepository = $fileRepository;
        $this->em = $em;
        $this->imageCombinerService = $imageCombinerService;
    }

    public function generateAtlases()
    {
        try {
            ini_set('max_execution_time', '500');
            $lastModificationOfArchetype = $this->archetypeRepository->findLatestImageModifiedDate();
            $lastModificationOfAtlases = $this->atlasRepository->findLatestModifiedDate();

            if ($lastModificationOfArchetype->getTimestamp() >= $lastModificationOfAtlases->getTimestamp()) {
                $images = [];

                foreach ($this->archetypeRepository->findAllArchetypes() as $archetype) {
                    if (!empty($archetype['image'])) {
                        $exploded = explode('/', $archetype['image']);
                        $id = end($exploded);

                        try {
                            /** @var FileEntity $file */
                            $file = $this->fileRepository->find($id);
                            $filePath = $this->cdnPath . $file->getName();
                            if (is_file($filePath)) {
                                $images[$archetype['id']] = $filePath;
                            }
                        } catch (EntityNotFoundException $e) {
                            // Actually do nothing
                        }
                    }
                }

                try {
                    $atlases = $this->imageCombinerService->imageCombine($images);
                } catch (ImageCombinerException $e) {
                    $atlases = [];
                }

                /** @var Atlas $atlas */
                foreach ($this->atlasRepository->findAll() as $atlas) {
                    // Remove all previous atlases
                    $this->em->remove($atlas);
                }

                foreach ($atlases as $atlas) {
                    if (isset($atlas['atlas'], $atlas['xml'])) {
                        $entity = new Atlas();
                        $entity->setImageAtlas($atlas['atlas']);
                        $entity->setXmlAtlas($atlas['xml']);

                        $this->em->persist($entity);
                    }
                }

                $this->em->flush();
            }
        } catch (\Exception $e) {
            throw new AtlasServiceException($e->getMessage());
        }
    }
}
