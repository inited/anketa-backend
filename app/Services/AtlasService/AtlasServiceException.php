<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 19. 02. 2019
 */

declare(strict_types=1);

namespace App\Services\AtlasService;

/**
 * Class AtlasServiceException
 * @package App\Services\AtlasService
 */
class AtlasServiceException extends \Exception
{

}
