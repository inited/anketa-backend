<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 19. 02. 2019
 */

declare(strict_types=1);

namespace App\Services\AtlasService;

/**
 * Interface AtlasServiceInterface
 * @package App\Services\AtlasService
 */
interface AtlasServiceInterface
{
    /**
     * @throws AtlasServiceException
     */
    public function generateAtlases();
}
