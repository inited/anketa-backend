<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\FileService;

use App\Model\Entity\FileEntity;
use App\Model\Repository\FileRepositoryInterface;
use Ramsey\Uuid\Uuid;
use Slim\Http\UploadedFile;

/**
 * Class FileService
 * @package App\Services\FileService
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class FileService implements FileServiceInterface
{

    /**
     * @var FileRepositoryInterface $fileRepository
     */
    private $fileRepository;

    /**
     * @var string $uploadDirectory
     */
    private $uploadDirectory;

    /**
     * FileService constructor.
     * @param string $uploadDirectory
     * @param FileRepositoryInterface $fileRepository
     */
    public function __construct(
        string $uploadDirectory,
        FileRepositoryInterface $fileRepository
    )
    {
        $this->fileRepository = $fileRepository;
        $this->uploadDirectory = $uploadDirectory;
    }

    /**
     * {@inheritdoc}
     */
    public
    function downloadFile(
        FileEntity $fileEntity
    )
    {
        if (!is_file($this->uploadDirectory . $fileEntity->getName())) {
            throw new FileNotFoundException();
        }

        header('Content-Description: File Transfer');
        header('Content-Type: ' . $fileEntity->getType());
        //header('Content-Disposition: attachment;filename="' . $fileEntity->getOriginalName() . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $fileEntity->getSize());
        readfile($this->uploadDirectory . $fileEntity->getName());
        exit;
    }

    /**
     * {@inheritdoc}
     */
    public function uploadFile(UploadedFile $file)
    {
        if ($file->getError() === UPLOAD_ERR_OK) {

            // Get extension
            $parseFileName = explode('.', $file->getClientFilename());
            $extension = end($parseFileName);
            $extension = $extension ? ('.' . strtolower($extension)) : '';

            $newFileName = Uuid::uuid4()->toString() . $extension;

            try {
                $file->moveTo($this->uploadDirectory . $newFileName);
                // Create new file entity
                $fileEntity = new FileEntity(
                    $newFileName,
                    $file->getClientFilename(),
                    $file->getSize(),
                    $file->getClientMediaType()
                );
                // Save and return file entity
                return $this->fileRepository->saveEntity($fileEntity);
            } catch (\Exception $e) {
                throw new FileServiceException($e->getMessage());
            }
        }

        throw new FileServiceException('Upload file failed!');
    }
}
