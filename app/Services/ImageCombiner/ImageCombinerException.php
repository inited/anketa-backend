<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 17. 02. 2019
 */

declare(strict_types=1);

namespace App\Services\ImageCombiner;

/**
 * Class ImageCombinerException
 * @package App\Services\ImageCombinerService
 */
class ImageCombinerException extends \Exception
{

}
