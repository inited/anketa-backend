<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 17. 02. 2019
 */

declare(strict_types=1);

namespace App\Services\ImageCombiner;

use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use Ramsey\Uuid\Uuid;

/**
 * Class ImageCombinerService
 * @package App\Services\ImageCombinerService
 */
class ImageCombinerService implements ImageCombinerServiceInterface
{
    const COL = 8;
    const ROW = 8;
    const THUMBNAIL_HEIGHT = 256;
    const THUMBNAIL_WIDTH = 256;

    /**
     * @var string
     */
    private $cdnPath;

    /**
     * @var string
     */
    private $tmpPath;

    /**
     * ImageCombinerService constructor.
     * @param string $cdnPath
     * @param string $tmpPath
     */
    public function __construct(
        string $cdnPath,
        string $tmpPath
    )
    {
        $this->cdnPath = rtrim($cdnPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->tmpPath = rtrim($tmpPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    /**
     * {@inheritdoc}
     */
    public function imageCombine(array $paths): array
    {
        try {
            $atlases = [];

            // Generate thumbnails from images
            $thumbnails = $this->prepareThumbnails($paths);
            $arrayChunk = array_chunk($thumbnails, self::COL * self::ROW, true);

            foreach ($arrayChunk as $i => $chunk) {
                try {
                    $x = $y = 0;
                    $defaultImage = $this->createDefaultImage();

                    $xml = simplexml_load_string('<TextureAtlas/>');

                    if (false === $xml) {
                        continue;
                    }

                    $xml->addAttribute('height', (string)(self::ROW * self::THUMBNAIL_HEIGHT));
                    $xml->addAttribute('width', (string)(self::COL * self::THUMBNAIL_WIDTH));

                    // Loop by link
                    $col = 0;
                    foreach ($chunk as $id => $path) {
                        if (self::COL === $col) {
                            $col = 0;
                            $x = 0;
                            $y += self::THUMBNAIL_HEIGHT;
                        }

                        // Image
                        $fileContents = file_get_contents($path);
                        if (false === $fileContents) {
                            throw new \Exception('file_get_contents returns false!');
                        }

                        $image = imagecreatefromstring($fileContents);

                        if (false === $image) {
                            continue;
                        }

                        $imagesX = imagesx($image);
                        $imagesY = imagesy($image);

                        /* Using imagecopy. */
                        imagecopy($defaultImage, $image, $x, $y, 0, 0, $imagesX, $imagesY);

                        // XML
                        $image = $xml->addChild('SubTexture');
                        $image->addAttribute('name', (string)$id);
                        $image->addAttribute('height', (string)self::THUMBNAIL_HEIGHT);
                        $image->addAttribute('width', (string)self::THUMBNAIL_WIDTH);
                        $image->addAttribute('y', (string)$y);
                        $image->addAttribute('x', (string)$x);

                        $col++;
                        $x += self::THUMBNAIL_WIDTH;

                    }

                    $uuid = Uuid::uuid4()->toString();
                    $xml->addAttribute('imagePath', $uuid . '.jpg');

                    $imagePath = $this->cdnPath . $uuid . '.jpg';
                    $xmlPath = $this->cdnPath . $uuid . '.xml';

                    imagejpeg($defaultImage, $imagePath);
                    $xml->saveXML($xmlPath);

                    $atlases[] = [
                        'atlas' => $uuid . '.jpg',
                        'xml' => $uuid . '.xml',
                    ];
                } catch (\Exception $e) {
                    // Do nothing
                }
            }

            return $atlases;
        } catch (\Exception $e) {
            throw new ImageCombinerException($e->getMessage());
        }
    }

    /**
     * @param string[] $paths
     * @return string[]
     */
    private function prepareThumbnails(array $paths): array
    {
        $thumbnails = [];
        foreach ($paths as $id => $img) {
            try {
                $exploded = explode(DIRECTORY_SEPARATOR, $img);
                $filename = end($exploded);
                $thumbnailPath = $this->tmpPath . $filename;
                if (!is_file($thumbnailPath)) {
                    $image = new ImageResize($img);
                    $image->crop(self::THUMBNAIL_WIDTH, self::THUMBNAIL_HEIGHT);
                    $image->save($thumbnailPath);
                }
                $thumbnails[$id] = $thumbnailPath;
            } catch (ImageResizeException $e) {
                // Do nothing
            }
        }

        return $thumbnails;
    }

    /**
     * @return resource
     * @throws \Exception
     */
    private function createDefaultImage()
    {
        $image = imagecreatetruecolor(self::COL * self::THUMBNAIL_WIDTH, self::ROW * self::THUMBNAIL_HEIGHT);

        if (false === $image) {
            throw new \Exception('imagecreatetruecolor failed!');
        }

        imagesavealpha($image, true);
        $transparent = imagecolorallocatealpha($image, 0, 0, 0, 127);
        imagefill($image, 0, 0, $transparent);

        return $image;
    }
}
