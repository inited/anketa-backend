<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 17. 02. 2019
 */

declare(strict_types=1);

namespace App\Services\ImageCombiner;

/**
 * Interface ImageCombinerServiceInterface
 * @package App\Services\ImageCombinerService
 */
interface ImageCombinerServiceInterface
{
    /**
     * @param array $paths
     * @return array
     * @throws ImageCombinerException
     */
    public function imageCombine(array $paths): array;
}
