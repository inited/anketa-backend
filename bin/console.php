#!/usr/bin/env php
<?php
// application.php
require __DIR__ . '/../vendor/autoload.php';

$settings = require __DIR__ . '/../src/settings.php';
if (is_file(__DIR__ . '/../src/settings.local.php')) {
    $settingsLocal = require __DIR__ . '/../src/settings.local.php';
    $settings = array_replace_recursive($settings, $settingsLocal);
}

$app = new \Slim\App($settings);
require __DIR__ . '/../src/dependencies.php';

$application = new \Symfony\Component\Console\Application();
$application->add($container->get('GenerateAtlasesCommand'));
$application->run();
