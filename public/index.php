<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

\Tracy\Debugger::enable(['217.170.105.135'], __DIR__ . '/../logs/', 'info@tomaspavlik.cz');
$debugMode = \Tracy\Debugger::detectDebugMode();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';

if ($debugMode && is_file(__DIR__ . '/../src/settings.local.php')) {
    $settingsLocal = require __DIR__ . '/../src/settings.local.php';
    $settings = array_replace_recursive($settings, $settingsLocal);
}

$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();

