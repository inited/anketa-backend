<?php

declare(strict_types=1);

// DIC configuration
use App\Command;
use App\Controllers;
use App\Model\Entity;
use App\Model\EntityListener;
use App\Model\Repository;
use App\Services;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container = $app->getContainer();

// view renderer
$container['renderer'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

# DOCTRINE ENTITY MANAGER
$container['em'] = function (ContainerInterface $container) {
    $settings = $container->get('settings')['doctrine'];
    $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
        $settings['meta']['entity_path'],
        $settings['meta']['auto_generate_proxies'],
        $settings['meta']['proxy_dir'],
        $settings['meta']['cache'],
        false
    );

    $em = \Doctrine\ORM\EntityManager::create($settings['connection'], $config);
    // Add custom functions
    $em->getConfiguration()->addCustomDatetimeFunction('unix_timestamp', \DoctrineExtensions\Query\Mysql\UnixTimestamp::class);
    // Register listeners
    $em->getConfiguration()->getEntityListenerResolver()->register($container->get('AtlasEntityListener'));
    $em->getConfiguration()->getEntityListenerResolver()->register($container->get('FileEntityListener'));

    // Return entity manager
    return $em;
};

# ENTITY LISTENERS
$container['AtlasEntityListener'] = function (ContainerInterface $container) {
    return new EntityListener\AtlasEntityListener(
        $container->get('settings')['atlasesDirectory'],
        $container->get('settings')['atlasDownloadUrl']
    );
};

$container['FileEntityListener'] = function (ContainerInterface $container) {
    return new EntityListener\FileEntityListener(
        $container->get('settings')['fileFullUrl']
    );
};

# COMMANDS
$container['GenerateAtlasesCommand'] = function (ContainerInterface $container) {
    return new Command\GenerateAtlasesCommand(
        $container->get('AtlasService')
    );
};

# CONTROLLERS
$container['ArchetypeController'] = function (ContainerInterface $container) {
    return new Controllers\ArchetypeController(
        $container->get('ArchetypeRepository')
    );
};

$container['AtlasesController'] = function (ContainerInterface $container) {
    return new Controllers\AtlasesController(
        $container->get('AtlasRepository')
    );
};

$container['AtlasesGenerateController'] = function (ContainerInterface $container) {
    return new Controllers\AtlasesGenerateController(
        $container->get('AtlasService')
    );
};

$container['ArchetypesController'] = function (ContainerInterface $container) {
    return new Controllers\ArchetypesController(
        $container->get('ArchetypeRepository')
    );
};

$container['ArchetypeCreateController'] = function (ContainerInterface $container) {
    return new Controllers\ArchetypeCreateController(
        $container->get('em')
    );
};

$container['ArchetypeDeleteController'] = function (ContainerInterface $container) {
    return new Controllers\ArchetypeDeleteController(
        $container->get('em'),
        $container->get('ArchetypeRepository')
    );
};

$container['ArchetypeUpdateController'] = function (ContainerInterface $container) {
    return new Controllers\ArchetypeUpdateController(
        $container->get('em'),
        $container->get('ArchetypeRepository')
    );
};

$container['FileController'] = function (ContainerInterface $container) {
    return new Controllers\FileController(
        $container->get('FileRepository'),
        $container->get('FileService'),
        $container->get('settings')['fileDownloadUrl']
    );
};

$container['FileUploadController'] = function (ContainerInterface $container) {
    return new Controllers\FileUploadController(
        $container->get('settings')['fileFullUrl'],
        $container->get('FileService')
    );
};

# REPOSITORY
$container['ArchetypeRepository'] = function (ContainerInterface $container) {
    return new Repository\ArchetypeRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Archetype::class)
    );
};

$container['AtlasRepository'] = function (ContainerInterface $container) {
    return new Repository\AtlasRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Atlas::class)
    );
};

$container['FileRepository'] = function (ContainerInterface $container) {
    return new Repository\FileRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\FileEntity::class)
    );
};

$container['RoleRepository'] = function (ContainerInterface $container) {
    return new Repository\RoleRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\RoleEntity::class)
    );
};

$container['TokenRepository'] = function (ContainerInterface $container) {
    return new Repository\TokenRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\TokenEntity::class)
    );
};

$container['UserRepository'] = function (ContainerInterface $container) {
    return new Repository\UserRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\UserEntity::class)
    );
};

# SERVICES
$container['AtlasService'] = function (ContainerInterface $container) {
    return new Services\AtlasService\AtlasService(
        $container->get('settings')['uploadsDirectory'],
        $container->get('ArchetypeRepository'),
        $container->get('AtlasRepository'),
        $container->get('em'),
        $container->get('FileRepository'),
        $container->get('ImageCombiner')
    );
};
$container['AuthenticationService'] = function (ContainerInterface $container) {
    return new Services\AuthenticationService\AuthenticationService(
        $container->get('TokenRepository'),
        $container->get('TokenStorage')
    );
};

$container['AdminAuthorizationService'] = function (ContainerInterface $container) {
    return new Services\AuthorizationService\AdminAuthorizationService(
        $container->get('TokenStorage')
    );
};
$container['FileService'] = function (ContainerInterface $container) {
    return new Services\FileService\FileService(
        $container->get('settings')['uploadsDirectory'],
        $container->get('FileRepository')
    );
};

$container['ImageCombiner'] = function (ContainerInterface $container) {
    return new Services\ImageCombiner\ImageCombinerService(
        $container->get('settings')['atlasesDirectory'],
        $container->get('settings')['tmpDirectory']
    );
};

$container['MailService'] = function (ContainerInterface $container) {
    return new Services\MailService\MailService(
        $container->get('renderer'),
        $container->get('settings')['mail']
    );
};

$container['TokenStorage'] = function () {
    return new App\Services\TokenStorage\TokenStorage();
};
