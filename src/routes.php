<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Routes
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @var \Slim\App $app
 * @var \Psr\Container\ContainerInterface $container
 */
$app->options('/{routes:.+}', function (Request $request, Response $response, array $args) {
    return $response;
});

// CORS
$app->add(function (Request $req, Response $res, App $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

// API
$app->group('/api', function () use ($container) {
    // Archetypes
    $this->delete('/archetype/{id}', 'ArchetypeDeleteController:defaultAction');
    $this->get('/archetype/{id}', 'ArchetypeController:defaultAction');
    $this->put('/archetype/{id}', 'ArchetypeUpdateController:defaultAction');
    $this->get('/archetypes', 'ArchetypesController:defaultAction');
    $this->post('/archetype', 'ArchetypeCreateController:defaultAction');
    $this->get('/atlas/download/{filename}', function (Request $req, Response $res, $args) use ($container) {
        $filename = $args['filename'];
        $path = $container->get('settings')['atlasesDirectory'];

        if (!empty($filename) && !empty($path) && is_file($path . $filename)) {
            $file = $path . $filename;
            $response = $res->withHeader('Content-Description', 'File Transfer')
                ->withHeader('Content-Type', 'application/octet-stream')
                ->withHeader('Content-Disposition', 'attachment;filename="' . basename($file) . '"')
                ->withHeader('Expires', '0')
                ->withHeader('Cache-Control', 'must-revalidate')
                ->withHeader('Pragma', 'public')
                ->withHeader('Content-Length', filesize($file));

            readfile($file);
            return $response;

        }

        return $res->withStatus(404);
    });
    $this->get('/atlas/generate', 'AtlasesGenerateController:defaultAction');
    $this->get('/atlas', 'AtlasesController:defaultAction');

    // Files
    $this->get('/file/{id}', 'FileController:defaultAction');
    $this->post('/file/upload', 'FileUploadController:defaultAction');
});
