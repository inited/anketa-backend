<?php

$serverUri = 'http://app.lykkell.dk/';
$apiUri = $serverUri . 'api/';

return [
    'settings' => [
        'serverUri' => $serverUri,
        'apiUri' => $apiUri,
        'fileFullUrl' => $apiUri . 'file/',
        'fileDownloadUrl' => $apiUri . 'uploads/',
        'uploadsDirectory' => __DIR__ . '/../public/uploads/',
        'atlasesDirectory' => __DIR__ . '/../public/atlases/',
        'atlasDownloadUrl' => $serverUri . 'api/atlas/download/',
        'tmpDirectory' => __DIR__ . '/../temp/',
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'debug' => false,

        'adminEmailForUserApproval' => 'jan@cabinplant.com',
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Mail
        'mail' => [
            'from' => 'no-reply@cabinplant.com',
            'fromName' => 'Cabinplant A/S',
            'smtp' => true,
            'host' => 'smtp.mandrillapp.com',
            'port' => 587,
            'username' => '30903043',
            'password' => 'Ysviak2ulMYZ2AY4_5PK4g',
        ],

        // Mpdf
        'mpdf' => [
            'tempDir' => __DIR__ . '/../temp',
            'dpi' => '150',
            'format' => 'A4-L',
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
            'templateModule' => 'productPdf/module.phtml',
            'templateProduct' => 'productPdf/product.phtml',
            'templateHeader' => 'productPdf/header.phtml',
            'templateFooter' => 'productPdf/footer.phtml',
        ],

        // Doctrine
        'doctrine' => [
            'connection' => [
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'dbname' => 'applykke_lykkell',
                'user' => 'applykke_lykkell',
                'password' => 'W.M_PmABIdOM',
                'charset' => 'utf8',
                'driverOptions' => array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            ],
            'meta' => [
                'entity_path' => [
                    'app/Model/Entity/'
                ],
                'auto_generate_proxies' => true,
                'cache' => null,
                'proxy_dir' => __DIR__ . '/../temp/cache/proxies',
                'customHydrationModes' => \Doctrine\ORM\Query::HYDRATE_ARRAY,
            ],
        ],
    ],
];
